![Alt AskMePortal](https://bitbucket.org/AleNitek/askmeportal/raw/5f71ffa15885480af65f2b9dabd267f89b1b0218/AskMePortal/wwwroot/img/AskMePortalLogo.png)

# AskMePortal
AskMePortal is a Web App made for training purposes that allows creating surveys and gathering answers for them.

## Url/Hosting

Solution is hosted on Azure using App Services functionality:  
[http://askmeportal2.azurewebsites.net/](http://askmeportal2.azurewebsites.net/)

## Version Check
[Version check](https://askmeportal2.azurewebsites.net/hc/version)

## Tech Stack
App:

* .Net Core 2.2 MVC
* T-SQL, server instance on Azure
* Front-end: Js, [Chart.js](https://www.chartjs.org/), jQuery, [jQueryUI](https://jqueryui.com/), [Boostrap](https://getbootstrap.com/), AdminLTE(https://adminlte.io/)

Deployment:

* using Azure DevOps

## Users

There are 3 types of users in portal:

* Super Admin - which allows creating users, surveys, assigning surveys to users and viewing results
* Admin - all above expect creating users
* User - recipient of survey, can only fill in assigned questionnaires

![Alt Roles and Users](https://bitbucket.org/AleNitek/askmeportal/raw/866e6e90d2aa7d6a0d57c9f96e9e751b6fcd1294/AskMePortal/wwwroot/img/users_and_roles.jpg)


## Logging

For right now application is using App Insights

More detail logs can be found here using KUDU project ([Kudu](https://github.com/projectkudu/kudu/wiki) is the engine behind git deployments in Azure Web Sites. 
It can also run outside of Azure):
[https://askmeportal2.scm.azurewebsites.net/](https://askmeportal2.scm.azurewebsites.net/)