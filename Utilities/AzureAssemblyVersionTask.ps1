############################################################
#########   Add AssemblyInformationalVersion        ########
############################################################

Write-Host "BuildNumber $Env:BuildNumber"
Write-Host "Branch $Env:SourceBranch"

$BuildCounter = "$Env:BuildNumber"
$pattern = '\[assembly: AssemblyInformationalVersion\("(.*)"\)\]'
$AssemblyFiles = Get-ChildItem . AssemblyInfo.cs -rec


foreach ($file in $AssemblyFiles)
{

(Get-Content $file.PSPath) | ForEach-Object{
    if($_ -match $pattern){
        # We have found the matching line
        # Edit the version number and put back.
        $fileVersion = [version]$matches[1]
        $newVersion = "{0}.{1}.{2}.{3}" -f $fileVersion.Major, $fileVersion.Minor, $BuildCounter, 0
        $infVersion = "Version: {0}, Branch: {1}" -f $Env:BuildNumber, $Env:SourceBranch
        '[assembly: AssemblyInformationalVersion(@"{0}")]' -f $infVersion
    } else {
        # Output line as is
        $_
    }
} | Set-Content $file.PSPath

}
Write-Host "[AssemblyInformationalVersion: '$infVersion']"