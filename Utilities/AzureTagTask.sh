#############################################################
###########    Add tag with BuildNumber    ##################
#############################################################

curl -X POST https://api.bitbucket.org/2.0/repositories/AleNitek/askmeportal/refs/tags -H 'Accept: */*' -H 'Accept-Encoding: gzip, deflate' -H 'Authorization: Basic xxxxxxxxx==' -H 'Cache-Control: no-cache' -H 'Connection: keep-alive' -H 'Host: api.bitbucket.org' -H 'Content-Type: application/json' --data '{"name":"'"$BuildNumber"'","target":{"hash":"'"$SourceVersion"'"}}'
