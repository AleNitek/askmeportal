﻿using AskMePortal.Data;
using AskMePortal.Models.Account;
using AskMePortal.Models.UsersProfiles;

namespace AskMePortal.Helpers
{
    public static class MapperExtensions
    {

        public static UsersProfilesVM MapToUsersProfilesVM(this ApplicationUser applicationUser)
        {
            var usersProfilesVM = new UsersProfilesVM()
            {
                Email = applicationUser.Email,
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                JobTitle = applicationUser.JobTitle,
                ProfileUrl = applicationUser.ProfileUrl,
                Avatar = applicationUser.AvatarUrl,
                CreatedDate = applicationUser.CreatedDate.ToString("f")
            };

            return usersProfilesVM;
        }

        public static ApplicationUser MapToApplicationUser(this AddUsersVM addUsersVM)
        {
            var user = new ApplicationUser
            {
                UserName = addUsersVM.Email,
                Email = addUsersVM.Email,
                FirstName = addUsersVM.FirstName,
                LastName = addUsersVM.LastName,
                JobTitle = addUsersVM.JobTitle,
                AvatarUrl = addUsersVM.AvatarUrl,
                ProfileUrl = addUsersVM.ProfileUrl,
                PhoneNumber = addUsersVM.PhoneNumber
            };

            return user;
        }
    }
}
