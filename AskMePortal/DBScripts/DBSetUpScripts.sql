﻿
IF USER_ID('askmeportaluser') IS NULL
BEGIN
CREATE USER askmeportaluser FROM LOGIN askmeportalapp;
ALTER ROLE db_datareader ADD MEMBER askmeportaluser; 
ALTER ROLE db_datawriter ADD MEMBER askmeportaluser; 
END

DROP TABLE IF EXISTS dbo.[Answer]
DROP TABLE IF EXISTS dbo.[Question]
DROP TABLE IF EXISTS dbo.[AssignedToUsers]
DROP TABLE IF EXISTS dbo.[Survey]
DROP TABLE IF EXISTS dbo.[AspNetUserTokens]
DROP TABLE IF EXISTS dbo.[AspNetUserRoles]
DROP TABLE IF EXISTS dbo.[AspNetUserLogins]
DROP TABLE IF EXISTS dbo.[AspNetUserClaims]
DROP TABLE IF EXISTS dbo.[AspNetRoleClaims]
DROP TABLE IF EXISTS dbo.[AspNetUsers]
DROP TABLE IF EXISTS dbo.[AspNetRoles]

CREATE TABLE [AspNetRoles]
([Id]               NVARCHAR(450) NOT NULL, 
 [Name]             NVARCHAR(256) NULL, 
 [NormalizedName]   NVARCHAR(256) NULL, 
 [ConcurrencyStamp] NVARCHAR(MAX) NULL, 
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY([Id])
);
GO
CREATE TABLE [AspNetUsers]
([Id]                   NVARCHAR(450) NOT NULL, 
 [FirstName]            NVARCHAR(256) NOT NULL, 
 [LastName]             NVARCHAR(256) NOT NULL, 
 [JobTitle]             NVARCHAR(256) NOT NULL, 
 [AvatarUrl]            NVARCHAR(400) NOT NULL, 
 [ProfileUrL]           NVARCHAR(400) NOT NULL, 
 [UserName]             NVARCHAR(256) NOT NULL, 
 [NormalizedUserName]   NVARCHAR(256) NULL, 
 [Email]                NVARCHAR(256) NOT NULL, 
 [NormalizedEmail]      NVARCHAR(256) NULL, 
 [CreatedDate]			DATETIME2 NOT NULL, 
 [EmailConfirmed]       BIT NOT NULL, 
 [PasswordHash]         NVARCHAR(MAX) NULL, 
 [SecurityStamp]        NVARCHAR(MAX) NULL, 
 [ConcurrencyStamp]     NVARCHAR(MAX) NULL, 
 [PhoneNumber]          NVARCHAR(MAX) NULL, 
 [PhoneNumberConfirmed] BIT NOT NULL, 
 [TwoFactorEnabled]     BIT NOT NULL, 
 [LockoutEnd]           DATETIMEOFFSET NULL, 
 [LockoutEnabled]       BIT NOT NULL, 
 [AccessFailedCount]    INT NOT NULL, 
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY([Id])
);
GO
CREATE TABLE [AspNetRoleClaims]
([Id]         INT NOT NULL IDENTITY, 
 [RoleId]     NVARCHAR(450) NOT NULL, 
 [ClaimType]  NVARCHAR(MAX) NULL, 
 [ClaimValue] NVARCHAR(MAX) NULL, 
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY([Id]), 
 CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId]) REFERENCES [AspNetRoles]([Id]) ON DELETE CASCADE
);
GO
CREATE TABLE [AspNetUserClaims]
([Id]         INT NOT NULL IDENTITY, 
 [UserId]     NVARCHAR(450) NOT NULL, 
 [ClaimType]  NVARCHAR(MAX) NULL, 
 [ClaimValue] NVARCHAR(MAX) NULL, 
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY([Id]), 
 CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [AspNetUsers]([Id]) ON DELETE CASCADE
);
GO
CREATE TABLE [AspNetUserLogins]
([LoginProvider]       NVARCHAR(128) NOT NULL, 
 [ProviderKey]         NVARCHAR(128) NOT NULL, 
 [ProviderDisplayName] NVARCHAR(MAX) NULL, 
 [UserId]              NVARCHAR(450) NOT NULL, 
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY([LoginProvider], [ProviderKey]), 
 CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [AspNetUsers]([Id]) ON DELETE CASCADE
);
GO
CREATE TABLE [AspNetUserRoles]
([UserId] NVARCHAR(450) NOT NULL, 
 [RoleId] NVARCHAR(450) NOT NULL, 
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY([UserId], [RoleId]), 
 CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId]) REFERENCES [AspNetRoles]([Id]) ON DELETE CASCADE, 
 CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [AspNetUsers]([Id]) ON DELETE CASCADE
);
GO
CREATE TABLE [AspNetUserTokens]
([UserId]        NVARCHAR(450) NOT NULL, 
 [LoginProvider] NVARCHAR(128) NOT NULL, 
 [Name]          NVARCHAR(128) NOT NULL, 
 [Value]         NVARCHAR(MAX) NULL, 
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY([UserId], [LoginProvider], [Name]), 
 CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [AspNetUsers]([Id]) ON DELETE CASCADE
);
GO
CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims]([RoleId]);
GO
CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles]([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
GO
CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims]([UserId]);
GO
CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins]([UserId]);
GO
CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles]([RoleId]);
GO
CREATE INDEX [EmailIndex] ON [AspNetUsers]([NormalizedEmail]);
GO
CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers]([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
GO

INSERT INTO AspNetRoles
VALUES
(1, 
 'SuperAdmin', 
 'SUPERADMIN', 
 NEWID()
);
  GO
INSERT INTO AspNetRoles
VALUES
(2, 
 'Admin', 
 'ADMIN', 
 NEWID()
);
  GO
INSERT INTO AspNetRoles
VALUES
(3, 
 'AppUser', 
 'APPUSER', 
 NEWID()
);
  GO

INSERT INTO AspNetUsers
VALUES
(NEWID(), 
'Cersei',
'Lannister',
'SuperAdmin',
'https://i.ibb.co/jGWcpJ3/Cersei.jpg',
'http://linkedin.com',
 'superadmin@askme.com', 
 'SUPERADMIN@ASKME.COM', 
 'superadmin@askme.com', 
 'SUPERADMIN@ASKME.COM', 
 GETDATE(),
 0, 
 'AQAAAAEAACcQAAAAEDL5uU6TyKyxiJ+QorszzwPdCohGISzQ4ZP1ZYBuhWLtxxUj3C5uVY6m0iF20pRV1g==', 
 NEWID(), 
 NEWID(), 
 NULL, 
 0, 
 0, 
 NULL, 
 1, 
 0
);
GO

INSERT INTO AspNetUsers
VALUES
(NEWID(), 
'Jon',
'Snow',
'Admin',
'https://i.ibb.co/P9DJ8zZ/Jon.jpg',
'http://linkedin.com',
 'admin@askme.com', 
 'ADMIN@ASKME.COM', 
 'admin@askme.com', 
 'ADMIN@ASKME.COM', 
 GETDATE(),
 0, 
 'AQAAAAEAACcQAAAAEDL5uU6TyKyxiJ+QorszzwPdCohGISzQ4ZP1ZYBuhWLtxxUj3C5uVY6m0iF20pRV1g==', 
 NEWID(), 
 NEWID(), 
 NULL, 
 0, 
 0, 
 NULL, 
 1, 
 0
);
GO

INSERT INTO AspNetUsers
VALUES
(NEWID(), 
'Arya',
'Stark',
'Dev',
'https://i.ibb.co/gScKVrN/Arya.jpg',
'http://linkedin.com',
 'appuser@askme.com', 
 'APPUSER@ASKME.COM', 
 'appuser@askme.com', 
 'APPUSER@ASKME.COM', 
 GETDATE(),
 0, 
 'AQAAAAEAACcQAAAAEDL5uU6TyKyxiJ+QorszzwPdCohGISzQ4ZP1ZYBuhWLtxxUj3C5uVY6m0iF20pRV1g==', 
 NEWID(), 
 NEWID(), 
 NULL, 
 0, 
 0, 
 NULL, 
 1, 
 0
);
GO

INSERT INTO AspNetUsers
VALUES
(NEWID(), 
'Eddard',
'Stark',
'Dev',
'https://i.ibb.co/N1yGbFT/Edward.jpg',
'http://linkedin.com',
 'appuser2@askme.com', 
 'APPUSER2@ASKME.COM', 
 'appuser2@askme.com', 
 'APPUSER2@ASKME.COM', 
 GETDATE(),
 0, 
 'AQAAAAEAACcQAAAAEDL5uU6TyKyxiJ+QorszzwPdCohGISzQ4ZP1ZYBuhWLtxxUj3C5uVY6m0iF20pRV1g==', 
 NEWID(), 
 NEWID(), 
 NULL, 
 0, 
 0, 
 NULL, 
 1, 
 0
);
GO

INSERT INTO AspNetUserRoles
VALUES
(
(
    SELECT au.Id
    FROM [dbo].[AspNetUsers] au
    WHERE au.Email = 'appuser@askme.com'
), 
(
    SELECT ar.Id
    FROM [dbo].[AspNetRoles] ar
    WHERE ar.Name = 'AppUser'
)
);
INSERT INTO AspNetUserRoles
VALUES
(
(
    SELECT au.Id
    FROM [dbo].[AspNetUsers] au
    WHERE au.Email = 'appuser2@askme.com'
), 
(
    SELECT ar.Id
    FROM [dbo].[AspNetRoles] ar
    WHERE ar.Name = 'AppUser'
)
);
INSERT INTO AspNetUserRoles
VALUES
(
(
    SELECT au.Id
    FROM [dbo].[AspNetUsers] au
    WHERE au.Email = 'admin@askme.com'
), 
(
    SELECT ar.Id
    FROM [dbo].[AspNetRoles] ar
    WHERE ar.Name = 'Admin'
)
);
INSERT INTO AspNetUserRoles
VALUES
(
(
    SELECT au.Id
    FROM [dbo].[AspNetUsers] au
    WHERE au.Email = 'superadmin@askme.com'
), 
(
    SELECT ar.Id
    FROM [dbo].[AspNetRoles] ar
    WHERE ar.Name = 'SuperAdmin'
)
);





CREATE TABLE [Survey] (
    [SurveyId] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [Created] datetime2 NOT NULL,
    [IsActive] bit NOT NULL,
	[CreatedBy] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_Survey] PRIMARY KEY ([SurveyId]),
    CONSTRAINT [FK_Survey_AspNetUsers_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
);

GO

CREATE TABLE [AssignedToUsers] (
    [AssignedToUsersId] int NOT NULL IDENTITY,
    [ApplicationUserId] nvarchar(450) NOT NULL,
    [IsCompleted] bit DEFAULT 0,
    [SurveyId] INT NOT NULL,
	[CompletedDate] datetime2 NULL,
    [AssignedDate] datetime2  NULL,

    CONSTRAINT [PK_AssignedToUsersId] PRIMARY KEY ([AssignedToUsersId]),
    CONSTRAINT [FK_AssignedToUsers_AspNetUsers_ApplicationUserId] FOREIGN KEY ([ApplicationUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_AssignedToUsers_Survey_SurveyId] FOREIGN KEY ([SurveyId]) REFERENCES [Survey] ([SurveyId]) ON DELETE NO ACTION

);


CREATE TABLE [Question] (
    [QuestionId] int NOT NULL IDENTITY,
    [Value] nvarchar(max) NULL,
    [QuestionType] int NOT NULL,
    [SurveyId] int NULL,
    CONSTRAINT [PK_Question] PRIMARY KEY ([QuestionId]),
    CONSTRAINT [FK_Question_Survey_SurveyId] FOREIGN KEY ([SurveyId]) REFERENCES [Survey] ([SurveyId]) ON DELETE NO ACTION
);

GO
CREATE INDEX [IX_Question_SurveyId] ON [Question] ([SurveyId]);

GO


CREATE TABLE [Answer] (
    [AnswerId] int NOT NULL IDENTITY,
    [SurveyId] int NOT NULL,
    [QuestionId] int NOT NULL,
    [ApplicationUserId] nvarchar(450) NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_Answer] PRIMARY KEY ([AnswerId]),
    CONSTRAINT [FK_Answer_AspNetUsers_ApplicationUserId] FOREIGN KEY ([ApplicationUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Answer_Question_QuestionId] FOREIGN KEY ([QuestionId]) REFERENCES [Question] ([QuestionId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Answer_Survey_SurveyId] FOREIGN KEY ([SurveyId]) REFERENCES [Survey] ([SurveyId]) ON DELETE CASCADE
);



INSERT INTO Survey 
VALUES ('Survey about work',GETDATE(),1,
(select u.Id from AspNetUsers u where u.Email ='admin@askme.com' ))
GO

INSERT INTO Question
VALUES
(
  'What do You like/dislike about your job?', 1, 1);

INSERT INTO Question
VALUES
(
  'What is your birth date?', 2, 1);

INSERT INTO Question
VALUES
(
  'How long have You been working for Your last company?', 3, 1);

INSERT INTO Question
VALUES
(
  'Do You like your job?', 4, 1);

GO


INSERT INTO AssignedToUsers
VALUES ( ( SELECT u.Id
           FROM AspNetUsers AS u
           WHERE email = 'appuser@askme.com'
         ) , 0,1,NULL,GETDATE()
       );

INSERT INTO AssignedToUsers
VALUES ( ( SELECT u.Id
           FROM AspNetUsers AS u
           WHERE email = 'appuser2@askme.com'
         ) , 0,1,NULL,GETDATE()
       );


