﻿$(function () {
    initDoughnutChart();
    initBarChart();
});


function initDoughnutChart() {
    var baseUrl = document.baseURI;
    $.ajax({
        type: "GET",
        url: baseUrl + "UserHome/GetDoughnutChartData",
        success: function (data) {
            if (data && data.chartData.reduce((a, b) => a + b, 0)) {
                createDoughnutChart(data);
            }
            else {
                createEmptyDoughnutChart()
            }
        }
    });
}

function initBarChart() {
    var baseUrl = document.baseURI;
    $.ajax({
        type: "GET",
        url: baseUrl + "UserHome/GetBarChartData",
        success: function (data) {
            createBarChart(data);
        }
    });
}

function createDoughnutChart(data) {
    var chart = new Chart(document.getElementById("doughnut-chart"), {
        type: 'doughnut',
        data: {
            labels: ["Active", "Answered", "Not Completed"],
            datasets: [
                {
                    backgroundColor: ["#3e95cd", "#8e5ea2", "#123456"],
                    data: data.chartData
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Surveys statistics'
            },
            responsive: false
        }
    });
}

function createEmptyDoughnutChart() {
    var chart = new Chart(document.getElementById("doughnut-chart"), {
        type: 'doughnut',
        data: {
            labels: ["No data ..."],
            datasets: [
                {
                    backgroundColor: ["#bebebe"],
                    data: [1]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Surveys statistics'
            },
            tooltips: {
                enabled: false
            },
            responsive: false
        }
    });
}

function createBarChart(data) {
    var chart = new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: data.labelsData,
            datasets: [
                {
                    backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#fd79a8", "#0984e3"],
                    data: data.chartData
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Assigned surveys in the last week'
            },
            responsive: false,
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        stepSize: 1
                    }
                }]
            }
        }
    });
}
