﻿function ShowInfoDialog() {
    $("#dialog-confirm").dialog({
        resizable: false,
        height: "auto",
        width: 320,
        modal: true,
        buttons:
        {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });
};