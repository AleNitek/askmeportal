﻿/** add active class and stay opened when selected */
var url = window.location + '';
$('ul.sidebar-menu a').filter(function () {
    var currentUrl = url.split('?')[0];
    return (currentUrl.toLowerCase().indexOf(this.href.toLowerCase()) >= 0);
}).parent().addClass('active');