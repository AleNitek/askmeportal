﻿$(function () {
    $('#newSurvey').validate();
    $('[data-toggle="tooltip"]').tooltip();

    $(".add-new").click(function () {
        addRow();
    });

    $(document).on("click", ".delete", function () {
        deleteRow(this);
    });
});

function addRow() {
    $("#AddSurvey").prop('disabled', false);
    var index = $("table tbody tr:last-child").index() + 1;
    var row = '<tr>' +
        '<td><input type="text" class="form-control" name="newSurveyVM.Questions[' + index + '].Value" required>' +
        '<td><select name="newSurveyVM.Questions[' + index + '].QuestionInputType" class="form-control">' +
        '<option value="1">textbox</option>' +
        '<option value="2">date</option>' +
        '<option value="3">number</option>' +
        '<option value="4">bool</option>' +
        '</select></td>' +
        '<td>' +
        '<a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
        '</td>' +
        '</tr>';
    $("table").append(row);
    $('[data-toggle="tooltip"]').tooltip();
}

function deleteRow(row) {
    $(row).tooltip('dispose');
    $(row).parents("tr").remove();
    var rowCount = $('#newSurveyTable tr').length;
    if (rowCount < 2) {
        $("#AddSurvey").prop('disabled', true);
        return;
    }
    recalcInputNames();
}

function recalcInputNames() {
    $("#newSurveyTable").find('tr').each(function (rowIndex) {
        $(this).find(':input').each(function () {
            var attrName = $(this).attr("name");
            var newAttrName = attrName.replace(/\[[0-9]\]/g, "[" + (rowIndex - 1) + "]");
            $(this).attr("name", newAttrName);
        });
    });
}