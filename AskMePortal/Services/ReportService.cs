﻿using System;
using System.Collections.Generic;
using System.Linq;
using AskMePortal.Data;
using AskMePortal.Models;

namespace AskMePortal.Services
{
    public class ReportService
    {
        private readonly ApplicationContext _applicationDbContext;
        public ReportService(ApplicationContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public List<BarChartReportVM> GetSurveysCountFromLastWeekByUserId(string userId)
        {
            var surveysInLastWeek = _applicationDbContext.AssignedToUsers
                .Where(x => (x.AssignedDate.Date > DateTime.Now.Date.AddDays(-7) && x.ApplicationUser.Id == userId))
                .GroupBy(u => u.AssignedDate.Date)
                .Select(o => new BarChartReportVM
                {
                    Day = o.Key,
                    SurveysCount = o.Count()
                });

            var report = new List<BarChartReportVM>();
            for (int i = 6; i >= 0; i--)
            {
                var dayTemplate = DateTime.Now.AddDays(-i).Date;
                var dayReport = surveysInLastWeek
                    .Select(y => new BarChartReportVM() { Day = y.Day, SurveysCount = y.SurveysCount }).FirstOrDefault(x => x.Day.Date == dayTemplate.Date);
                report.Add(dayReport ?? new BarChartReportVM() { Day = dayTemplate, SurveysCount = 0 });
            }

            return report;
        }

        public DoughnutChartReportVM GetSurveysStatsByUserId(string userId)
        {
            var userSurveys = _applicationDbContext.AssignedToUsers
                .Where(x => (x.ApplicationUser.Id == userId));
            int activeSurveys = userSurveys.Count(x => x.Survey.IsActive && !x.IsCompleted);
            int answeredSurveys = userSurveys.Count(x => (x.ApplicationUser.Id == userId) && x.IsCompleted);
            int notCompleted = userSurveys.Count(x => !x.IsCompleted && !x.Survey.IsActive);

            var reportVM = new DoughnutChartReportVM()
            {
                Active = activeSurveys,
                Answered = answeredSurveys,
                NotCompleted = notCompleted
            };

            return reportVM;
        }
    }
}
