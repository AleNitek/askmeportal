﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using AskMePortal.Data;
using AskMePortal.Data.DTO;
using AskMePortal.Models;
using AskMePortal.Models.ActiveSurveys;
using AskMePortal.Models.AssignToUsers;
using AskMePortal.Models.NewSurvey;
using AskMePortal.Models.Results;
using AskMePortal.Models.SurveysList;

namespace AskMePortal.Services
{
    public class SurveyService
    {
        private readonly ApplicationContext _applicationDbContext;
        public SurveyService(ApplicationContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<SurveysListVM> GetAllSurveys()
        {
            var surveys = _applicationDbContext.Surveys.AsNoTracking().Select(s => new SurveysListVM()
            {
                Id = s.SurveyId,
                IsActive = s.IsActive,
                Name = s.Name,
                CreatedBy = s.ApplicationUser.Email,
                CreatedDate = s.Created,
                Questions = s.Questions.Select(q => new QuestionVM()
                {
                    QuestionId = q.QuestionId,
                    SurveyId = q.Survey.SurveyId,
                    QuestionInputType = q.QuestionType,
                    Value = q.Value
                }).ToList()
            });

            return surveys;
        }

        public IEnumerable<SurveysListVM> GetActiveSurveys()
        {
            var surveys = GetAllSurveys().Where(s => s.IsActive);
            return surveys;
        }

        public SurveyVM GetActiveSurveyById(int surveyId, string userId)
        {
            var survey = GetActiveSurveysByUserId(userId).FirstOrDefault(x => x.SurveyId == surveyId);
            return survey;
        }

        public IEnumerable<ActiveSurveysVM> GetFillableSurveysById(string userId)
        {
            var fillable = _applicationDbContext.AssignedToUsers.AsNoTracking()
                .Where(u => (u.ApplicationUserId == userId && u.Survey.IsActive && !u.IsCompleted))
                   .Select(s => new ActiveSurveysVM()
                   {
                       Id = s.SurveyId,
                       Name = s.Survey.Name,
                       Created = s.Survey.Created,
                       IsCompleted = s.IsCompleted
                   });

            return fillable;
        }

        public bool CreateSurvey(NewSurveyVM newSurveyVM, string userId)
        {
            var questionsDto = newSurveyVM.Questions.Select(q => new Question()
            {
                QuestionType = q.QuestionInputType,
                Value = q.Value
            });

            var surveyDTO = new Survey()
            {
                Name = newSurveyVM.SurveyName,
                Created = DateTime.UtcNow,
                IsActive = true,
                CreatedBy = userId,
                Questions = questionsDto.ToList()
            };
            _applicationDbContext.Surveys.Add(surveyDTO);
            int result = _applicationDbContext.SaveChanges();
            return result > 0;
        }

        public bool AssignSurveyToUsers(int surveyId, List<UserVM> usersVM)
        {
            var assignDTO = usersVM.Where(x => x.IsSelected)
                .Select(u => new AssignedToUsers()
                {
                    ApplicationUserId = u.Id,
                    SurveyId = surveyId,
                    AssignedDate = DateTime.Now
                });

            foreach (var dto in assignDTO)
            {
                if (!_applicationDbContext.AssignedToUsers.Any(x => (x.SurveyId == dto.SurveyId && x.ApplicationUserId == dto.ApplicationUserId)))
                {
                    _applicationDbContext.AssignedToUsers.Add(dto);
                }
            }

            int result = _applicationDbContext.SaveChanges();
            return result > 0;
        }

        public IEnumerable<string> GetAssignedSurveysByUserId(string userId)
        {
            var alreadyAssigned = _applicationDbContext.AssignedToUsers.AsNoTracking()
                .Where(u => u.ApplicationUserId == userId)
                .Select(s => ($"{s.Survey.Name} ({(s.IsCompleted ? "completed" : "not completed")})"))
                .ToList();

            if (!alreadyAssigned.Any())
            {
                alreadyAssigned = new List<string>() { "No surveys yet" };
            }

            return alreadyAssigned;
        }

        public bool CloseSurvey(int surveyId)
        {
            var surveyToClose = _applicationDbContext.Surveys.FirstOrDefault(s => s.SurveyId == surveyId);
            if (surveyToClose != null)
            {
                surveyToClose.IsActive = false;
                _applicationDbContext.Update(surveyToClose);
            }

            int result = _applicationDbContext.SaveChanges();
            return result > 0;
        }

        public bool SaveAnswers(SurveyVM surveyVM, string userId)
        {
            var answers = surveyVM.Questions.Select(q => new Answer()
            {
                ApplicationUserId = userId,
                QuestionId = q.QuestionId,
                SurveyId = surveyVM.SurveyId,
                Value = q.QuestionAnswer
            });

            _applicationDbContext.Answers.AddRange(answers);
            var surveyToMarkAsCompleted = _applicationDbContext.AssignedToUsers.FirstOrDefault(s => (s.SurveyId == surveyVM.SurveyId && s.ApplicationUserId == userId));

            if (surveyToMarkAsCompleted != null)
            {
                surveyToMarkAsCompleted.IsCompleted = true;
                surveyToMarkAsCompleted.CompletedDate = DateTime.Now;
                _applicationDbContext.Update(surveyToMarkAsCompleted);
            }

            int result = _applicationDbContext.SaveChanges();
            return result > 0;
        }

        public IEnumerable<ResultsQuestionsVM> GetResultsBySurveyId(int surveyId)
        {
            var resultAnswers = _applicationDbContext.Answers.AsNoTracking()
                .Where(x => (x.SurveyId == surveyId));
            if (!resultAnswers.Any())
            {
                return new List<ResultsQuestionsVM>();
            }

            var answersForSurvey = _applicationDbContext.Question.AsNoTracking()
                .Where(a => a.SurveyId == surveyId)
                .Select(r => new ResultsQuestionsVM()
                {
                    QuestionId = r.QuestionId,
                    QuestionValue = r.Value,
                    QuestionType = r.QuestionType.ToString(),
                    ResultAnswers = resultAnswers.Where(x => x.QuestionId == r.QuestionId)
                    .Select(x => new ResultsAnswersVM()
                    {
                        AnswerValue = x.Value,
                        UserEmail = x.ApplicationUser.Email
                    }).ToList()
                });

            return answersForSurvey;
        }

        private IEnumerable<SurveyVM> GetActiveSurveysByUserId(string userId)
        {
            var surveys = _applicationDbContext.Surveys.AsNoTracking()
                .Select(s => new SurveyVM()
                {
                    Created = s.Created,
                    IsActive = s.IsActive,
                    Name = s.Name,
                    SurveyId = s.SurveyId,
                    Questions = s.Questions.Select(q => new QuestionVM()
                    {
                        QuestionId = q.QuestionId,
                        SurveyId = q.Survey.SurveyId,
                        QuestionInputType = q.QuestionType,
                        Value = q.Value,
                        QuestionAnswer = _applicationDbContext.Answers
                            .FirstOrDefault(y => (y.QuestionId == q.QuestionId && y.SurveyId == s.SurveyId
                                                                               && y.ApplicationUser.Id == userId)).Value
                    }).Where(q => q.SurveyId == s.SurveyId).ToList()

                }).Where(y => y.IsActive);

            return surveys;
        }
    }
}
