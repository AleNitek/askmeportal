﻿using System;

namespace AskMePortal.Models.ActiveSurveys
{
    public class ActiveSurveysVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsCompleted { get; set; }
    }
}
