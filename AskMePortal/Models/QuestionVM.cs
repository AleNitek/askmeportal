﻿using AskMePortal.Helpers;

namespace AskMePortal.Models
{
    public class QuestionVM
    {
        public int QuestionId { get; set; }
        public string Value { get; set; }
        public QuestionType QuestionInputType { get; set; }
        public string QuestionAnswer { get; set; }
        public int SurveyId { get; set; }
    }
}
