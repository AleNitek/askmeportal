﻿namespace AskMePortal.Helpers
{
    public enum QuestionType
    {
        text = 1,
        date = 2,
        number = 3,
        checkbox = 4
    }
}
