﻿namespace AskMePortal.Models.UsersProfiles
{
    public class UsersProfilesVM
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string ProfileUrl { get; set; }
        public string Avatar { get; set; }
        public string CreatedDate { get; set; }
    }
}
