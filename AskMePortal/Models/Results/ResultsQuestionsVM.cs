﻿using System.Collections.Generic;

namespace AskMePortal.Models.Results
{
    public class ResultsQuestionsVM
    {
        public int QuestionId { get; set; }
        public string QuestionValue { get; set; }
        public string QuestionType { get; set; }
        public List<ResultsAnswersVM> ResultAnswers { get; set; }
    }
}
