﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace AskMePortal.Models.Results
{
    public class ResultsVM
    {
        public List<SelectListItem> AllSurveys { get; set; }
        public List<ResultsQuestionsVM> ResultsQuestions { get; set; }
    }
}
