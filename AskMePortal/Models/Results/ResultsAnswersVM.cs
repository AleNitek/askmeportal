﻿namespace AskMePortal.Models.Results
{
    public class ResultsAnswersVM
    {
        public string AnswerValue { get; set; }
        public string UserEmail { get; set; }
    }
}
