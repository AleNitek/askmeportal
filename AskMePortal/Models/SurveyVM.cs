﻿using System;
using System.Collections.Generic;

namespace AskMePortal.Models
{
    public class SurveyVM
    {
        public int SurveyId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsActive { get; set; }
        public List<QuestionVM> Questions { get; set; }
    }
}
