﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AskMePortal.Data;

namespace AskMePortal.Models.NewSurvey
{
    public class NewSurveyVM
    {
        [Required]
        public string SurveyName { get; set; }
        public List<QuestionVM> Questions { get; set; }
    }
}
