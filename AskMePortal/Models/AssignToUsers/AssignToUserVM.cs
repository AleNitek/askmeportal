﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace AskMePortal.Models.AssignToUsers
{
    public class AssignToUserVM
    {
        public List<UserVM> AssignUsersVM { get; set; }
        public List<SelectListItem> SurveysToAssign { get; set; }
        public int SurveyId { get; set; }
    }
}
