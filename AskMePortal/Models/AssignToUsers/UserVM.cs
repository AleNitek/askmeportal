﻿using System.Collections.Generic;

namespace AskMePortal.Models.AssignToUsers
{
    public class UserVM
    {
        public bool IsSelected { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public List<string> AlreadyAssignedSurveys { get; set; }

    }
}
