﻿using System;
namespace AskMePortal.Models
{
    public class BarChartReportVM
    {
        public DateTime Day { get; set; }
        public int SurveysCount { get; set; }
    }
}
