﻿using System;
using System.Collections.Generic;

namespace AskMePortal.Models.SurveysList
{
    public class SurveysListVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<QuestionVM> Questions { get; set; }
    }
}
