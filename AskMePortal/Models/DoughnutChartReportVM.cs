﻿namespace AskMePortal.Models
{
    public class DoughnutChartReportVM
    {
        public int Answered { get; set; }
        public int Active { get; set; }
        public int NotCompleted { get; set; }
    }
}
