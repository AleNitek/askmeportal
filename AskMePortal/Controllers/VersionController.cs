﻿using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AskMePortal.Controllers
{
    public class VersionController : Controller
    {
        private static string GetCurrentVersion()
        {
            var versionAttribute = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            return versionAttribute?.InformationalVersion ?? "[Unkown]";
        }

        [AllowAnonymous]
        [HttpGet, Route("hc/version")]
        public JsonResult Version()
        {
            return new JsonResult(GetCurrentVersion());
        }
    }
}
