﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using AskMePortal.Data;
using AskMePortal.Models;
using AskMePortal.Services;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "AppUser")]
    public class ActiveSurveysController : Controller
    {
        private readonly SurveyService _surveyService;
        private readonly UserManager<ApplicationUser> _userManager;

        public ActiveSurveysController(SurveyService surveyService, UserManager<ApplicationUser> userManager)
        {
            _surveyService = surveyService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var userId = _userManager.GetUserId(User);
            var activeSurveys = _surveyService.GetFillableSurveysById(userId).ToList();
            return View(activeSurveys);
        }

        [HttpGet]
        public IActionResult GetSurvey(int surveyId)
        {
            var userId = _userManager.GetUserId(User);
            var activeSurvey = _surveyService.GetActiveSurveyById(surveyId, userId);
            return View("Survey", activeSurvey);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitSurvey(SurveyVM surveyVM)
        {
            var userId = _userManager.GetUserId(User);
            bool result = _surveyService.SaveAnswers(surveyVM, userId);
            var fillableSurveys = _surveyService.GetFillableSurveysById(userId).ToList();
            ViewBag.Message = result ? "Answers were saved successfully!" : "Something went wrong when saving answers...";
            return View("Index", fillableSurveys);
        }
    }
}