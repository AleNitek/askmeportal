﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using AskMePortal.Data;
using AskMePortal.Models.Account;
using AskMePortal.Helpers;
using System;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class AddUsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AddUsersController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult AddUsers()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUsers(AddUsersVM model)
        {
            if (!(model.UserType == ApplicationUserRoles.AppUser.ToString() || model.UserType == ApplicationUserRoles.Admin.ToString()))
            {
                ModelState.AddModelError(string.Empty, "User type is invalid!");
                return View(model);
            }
            if (ModelState.IsValid)
            {
                var user = model.MapToApplicationUser();
                user.CreatedDate = DateTime.Now;
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, model.UserType);
                    ModelState.Clear();
                    ViewBag.Message = "Hurray user created!";
                    return View();
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

    }
}