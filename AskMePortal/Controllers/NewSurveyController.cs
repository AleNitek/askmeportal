﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using AskMePortal.Data;
using AskMePortal.Models.NewSurvey;
using AskMePortal.Services;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class NewSurveyController : Controller
    {
        private readonly SurveyService _surveyService;
        private readonly UserManager<ApplicationUser> _userManager;

        public NewSurveyController(SurveyService surveyService, UserManager<ApplicationUser> userManager)
        {
            _surveyService = surveyService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(NewSurveyVM newSurveyVM)
        {
            var userId = _userManager.GetUserId(User);
            bool result = _surveyService.CreateSurvey(newSurveyVM, userId);
            ViewBag.Message = result ? "New Survey was created !" : "Something went wrong...";
            return View("Index");
        }
    }
}