﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using AskMePortal.Data;
using AskMePortal.Services;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "AppUser")]
    public class UserHomeController : Controller
    {
        private readonly ReportService _reportService;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserHomeController(ReportService reportService, UserManager<ApplicationUser> userManager)
        {
            _reportService = reportService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDoughnutChartData()
        {
            var userId = _userManager.GetUserId(User);
            var data = _reportService.GetSurveysStatsByUserId(userId);
            var chartData = new int[] { data.Active, data.Answered, data.NotCompleted };
            return new JsonResult(new { chartData });
        }

        [HttpGet]
        public JsonResult GetBarChartData()
        {
            var userId = _userManager.GetUserId(User);
            var data = _reportService.GetSurveysCountFromLastWeekByUserId(userId);
            var chartData = data.Select(x => x.SurveysCount).ToArray();
            var labelsData = data.Select(x => x.Day.ToShortDateString()).ToArray();
            return new JsonResult(new { chartData, labelsData });
        }
    }
}