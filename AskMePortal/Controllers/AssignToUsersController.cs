﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using AskMePortal.Data;
using AskMePortal.Models.AssignToUsers;
using AskMePortal.Services;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AssignToUsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SurveyService _surveyService;

        public AssignToUsersController(UserManager<ApplicationUser> userManager, SurveyService surveyService)
        {
            _userManager = userManager;
            _surveyService = surveyService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var assignToUsersVM = await GetAssignToUserVM();
            return View(assignToUsersVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Assign(AssignToUserVM assignToUserVM)
        {
            bool isAnyUserSelected = assignToUserVM.AssignUsersVM.Any(x => x.IsSelected);
            if (isAnyUserSelected)
            {
                var result = _surveyService.AssignSurveyToUsers(assignToUserVM.SurveyId, assignToUserVM.AssignUsersVM);
                ViewBag.Message = result ? "Surveys were assigned successfully!" : "No new surveys were assigned";
                ModelState.Clear();
            }
            else
            {
                ViewBag.Message = "No users were selected to assign";
            }

            var assignToUsersVM = await GetAssignToUserVM();
            return View("Index", assignToUsersVM);
        }

        private async Task<AssignToUserVM> GetAssignToUserVM()
        {
            var users = await _userManager.GetUsersInRoleAsync("AppUser");

            var assignUsersVM = users.Select(u => new UserVM()
            {
                IsSelected = false,
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                JobTitle = u.JobTitle,
                AlreadyAssignedSurveys = _surveyService.GetAssignedSurveysByUserId(u.Id).ToList()
            });

            List<SelectListItem> options = _surveyService.GetActiveSurveys().Select(a =>
                                         new SelectListItem
                                         {
                                             Value = a.Id.ToString(),
                                             Text = a.Name
                                         }).ToList();

            var assignToUsersVM = new AssignToUserVM()
            {
                AssignUsersVM = assignUsersVM.ToList(),
                SurveysToAssign = options
            };

            return assignToUsersVM;
        }
    }
}