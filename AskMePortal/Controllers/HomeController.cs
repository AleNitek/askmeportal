﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace AskMePortal.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("LogIn", "Account");

            }
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return RedirectToAction("Index", "NewSurvey");
            }
            if (User.IsInRole("AppUser"))
            {
                return RedirectToAction("Index", "UserHome");
            }

            return RedirectToAction("LogIn", "Account");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int? statusCode)
        {
            if (statusCode != null && statusCode == (int)HttpStatusCode.NotFound)
            {
                return View("NotFound");
            }
            return View("Error");
        }
    }
}
