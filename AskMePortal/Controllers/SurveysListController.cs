﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using AskMePortal.Services;
using System.Collections.Generic;
using AskMePortal.Models.SurveysList;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class SurveysListController : Controller
    {
        private readonly SurveyService _surveyService;

        public SurveysListController(SurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var surveys = OrderSurveys(_surveyService.GetAllSurveys()).ToList();
            return View(surveys);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CloseSurvey(int surveyId)
        {
            bool result = _surveyService.CloseSurvey(surveyId);
            ViewBag.Message = result ? "Survey was closed successfully" : "Closing survey has failed";
            var surveys = OrderSurveys(_surveyService.GetAllSurveys()).ToList();
            return View("Index", surveys);
        }

        public IEnumerable<SurveysListVM> OrderSurveys(IEnumerable<SurveysListVM> surveys)
        {
            var orderedSurveys = surveys.OrderByDescending(x => x.CreatedDate).ThenByDescending(x => x.IsActive);
            return orderedSurveys;
        }
    }
}