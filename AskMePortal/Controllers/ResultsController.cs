﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using AskMePortal.Models.Results;
using AskMePortal.Services;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class ResultsController : Controller
    {
        private readonly SurveyService _surveyService;

        public ResultsController(SurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [HttpGet]
        public IActionResult Index(int? surveyId)
        {
            var resultVM = new ResultsVM()
            {
                AllSurveys = _surveyService.GetAllSurveys().Select(a =>
                                         new SelectListItem
                                         {
                                             Value = a.Id.ToString(),
                                             Text = a.Name
                                         }).ToList()
            };

            if (surveyId == null)
            {
                return View(resultVM);
            }

            resultVM.ResultsQuestions = _surveyService.GetResultsBySurveyId(surveyId.Value).ToList();

            if (!resultVM.ResultsQuestions.Any())
            {
                ViewBag.Message = "There are no results for chosen survey yet...";
            }

            return View(resultVM);
        }
    }
}