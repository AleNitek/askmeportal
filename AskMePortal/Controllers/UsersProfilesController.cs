﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using AskMePortal.Data;
using AskMePortal.Helpers;

namespace AskMePortal.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class UsersProfiles : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UsersProfiles(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var users = await _userManager.GetUsersInRoleAsync("AppUser");

            var usersVM = users.Select(u => u.MapToUsersProfilesVM());

            if (User.IsInRole("SuperAdmin"))
            {
                var adminUsers = await _userManager.GetUsersInRoleAsync("Admin");
                var adminUsersVM = adminUsers.Select(u => u.MapToUsersProfilesVM());
                usersVM = usersVM.Concat(adminUsersVM);
            }

            return View(usersVM);
        }
    }
}
