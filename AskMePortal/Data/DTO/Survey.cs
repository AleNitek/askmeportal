﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskMePortal.Data.DTO
{
    [Table("Survey")]
    public class Survey
    {
        [Key]
        public int SurveyId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser ApplicationUser { get; set; }

        [InverseProperty("Survey")]
        public ICollection<AssignedToUsers> AssignedToUsers { get; set; }

        [InverseProperty("Survey")]
        public ICollection<Question> Questions { get; set; }

    }
}
