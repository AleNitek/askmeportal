﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskMePortal.Data.DTO
{
    [Table("AssignedToUsers")]
    public class AssignedToUsers
    {
        [Key]
        public int AssignedToUsersId { get; set; }
        public string ApplicationUserId { get; set; }
        public int SurveyId { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime AssignedDate { get; set; }
        public DateTime? CompletedDate { get; set; }

        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("SurveyId")]
        public Survey Survey { get; set; }
    }
}
