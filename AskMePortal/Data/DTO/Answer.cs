﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskMePortal.Data.DTO
{
    [Table("Answer")]
    public class Answer
    {
        [Key]
        public int AnswerId { get; set; }
        public string Value { get; set; }


        public int SurveyId { get; set; }
        [ForeignKey("SurveyId ")]
        public Survey Survey { get; set; }

        public int QuestionId { get; set; }
        [ForeignKey("QuestionId")]
        public Question Question { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
