﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AskMePortal.Helpers;

namespace AskMePortal.Data.DTO
{
    [Table("Question")]
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }
        public string Value { get; set; }
        public QuestionType QuestionType { get; set; }
        public int SurveyId { get; set; }

        [ForeignKey("SurveyId")]
        public Survey Survey { get; set; }
    }
}
