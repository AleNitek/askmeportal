﻿using Microsoft.AspNetCore.Identity;
using System;

namespace AskMePortal.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string AvatarUrl { get; set; }
        public string ProfileUrl { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum ApplicationUserRoles
    {
        SuperAdmin,
        Admin,
        AppUser
    }
}
